﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Utility
{
  public class Lookups
  {
    //Type of appointment
    public enum AppointmentTypes
    {
      [Description("Personal - New Checking")]
      PersonalNewChecking = 1,

      [Description("Personal - New Mortgage")]
      PersonalNewMortgage = 2,

      [Description("Personal - New Loan")]
      PersonalNewLoan = 3,

      [Description("Personal - New Credit Card")]
      PersonalNewCreditCard = 4,

      [Description("Personal - Service Existing Account")]
      PersonalServiceExistingAccount = 5,

      [Description("Personal - Investments")]
      PersonalInvestments = 6,

      [Description("Personal - Retirement Planning")]
      PersonalRetirementPlanning = 7,

      [Description("Personal - 529 College Savings Plans (NEST")]
      PersonalCollegeSavingsPlans = 8,

      [Description("Business - New Checking")]
      BusinessNewChecking = 9,

      [Description("Business - New Savings")]
      BusinessNewSavings = 10,

      [Description("Business - New Money Market")]
      BusinessNewMoneyMarket = 11,

      [Description("Business - New CDs")]
      BusinessNewCDs = 12,

      [Description("Business - New Loan")]
      BusinessNewLoan = 13,

      [Description("Business - New Credit Card")]
      BusinessNewCreditCard = 14,

      [Description("Business - Service Existing Account")]
      BusinessServiceExistingAccount = 15
    }

    public static string GetDescription(Enum en)
    {
      var type = en.GetType();
      var memberInfo = type.GetMember(en.ToString());

      if (memberInfo != null && memberInfo.Length > 0)
      {
        var appts = memberInfo[0].GetCustomAttributes(typeof (DescriptionAttribute), false);

        if (appts != null && appts.Length > 0)
        {
          return ((DescriptionAttribute) appts[0]).Description;
        }
      }

      return en.ToString();
    }
  }
}