﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace AppointmentScheduler.Utility
{
  public static class Mailer
  {
    public static void SendEmail(string from, string to, string subject, string body)
    {
      var mail = new MailMessage();

      mail.To.Add(to);

      mail.From = new MailAddress(from);
      mail.Subject = subject;
      mail.Body = body;
      mail.IsBodyHtml = false;
      
      var smtp = new SmtpClient();
      
      smtp.Host = "127.0.0.1";
      smtp.Port = 25;
      smtp.UseDefaultCredentials = true;
      //smtp.UseDefaultCredentials = false;
      //smtp.Credentials = new System.Net.NetworkCredential("username", "password");// Enter seders User name and password

      smtp.EnableSsl = false;
      //smtp.EnableSsl = true;

      smtp.Send(mail);
    }
  }
}