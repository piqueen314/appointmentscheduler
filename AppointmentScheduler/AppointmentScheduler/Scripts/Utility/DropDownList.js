﻿DropDownListUtility = (function () {
  var exports = {};

  exports.dropDownList = function (ddlId, keyId) {
    $("#" + ddlId).change(function () {
      //keeps the hidden Id value synced up with the selected thing in the drop down
      $("#" + keyId).val($("#" + ddlId + " option:selected").val());
    }).change();
  }

  return exports;
})();
