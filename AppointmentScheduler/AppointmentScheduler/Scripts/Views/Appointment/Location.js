﻿$(function () {
  DropDownListUtility.dropDownList("ddlLocation", "LocationId");

  //show/hide location details based on the selected location in ddlLocation
  $("#ddlLocation").change(function () {
    //var element = this;

    //the id of the selected location
    var selectedLocationId = $("#ddlLocation option:selected").val();

    //the location details you want to show
    var locationToShow = $("#AppointmentLocations").children("[data-LocationId='" + selectedLocationId + "']");

    locationToShow.show();

    //the location(s) details you want to hide
    var locationsToHide = $("#AppointmentLocations").children("[data-LocationId!='" + selectedLocationId + "']");

    locationsToHide.each(function () {
      $(this).hide();
    })
  }).change();
});

$(function () {
  DropDownListUtility.dropDownList("ddlLocation", "LocationId");

  $("#btnContinueLocation").on("click", function () {

    if ($("#LocationId").val() > 0) {
      //the value is there, submit the form
      return true;
    } else {
      //the value is not there, alert
      alert("Please specify a location.");

      return false;
    }
  });
});