﻿$(function() {
  DropDownListUtility.dropDownList("ddlAppointmentType", "AppointmentTypeId");

  $("#btnContinueReason").on("click", function() {

    if ($("#AppointmentTypeId").val() > 0) {
      //the value is there, submit the form
      return true;
    } else {
      //the value is not there, alert
      alert("Please specify an appointment type.");

      return false;
    }
  });
});