﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Models
{
  public class AppointmentDocument
  {
    public int AppointmentDocumentId { get; set; }

    public int AppointmentTypeId { get; set; }

    [Required]
    public string Name { get; set; }

    public virtual AppointmentType AppointmentType { get; set; }
  }
}