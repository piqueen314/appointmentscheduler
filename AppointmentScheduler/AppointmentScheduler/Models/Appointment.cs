﻿using System;
using System.ComponentModel;

namespace AppointmentScheduler.Models
{
  public class Appointment
  {
    public int AppointmentId { get; set; }
    
    public int AppointmentTypeId { get; set; }
    
    public int? CustomerId { get; set; }
    
    public int? LocationId { get; set; }

    public DateTime? StartTime { get; set; }

    public string Notes { get; set; }

    public virtual AppointmentType AppointmentType { get; set; }

    public virtual Customer Customer { get; set; }

    public virtual Location Location { get; set; }
  }
}