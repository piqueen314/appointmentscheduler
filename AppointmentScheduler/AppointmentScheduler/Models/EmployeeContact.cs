﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Models
{
  public class EmployeeContact
  {
    public int EmployeeContactId { get; set; }

    public int LocationId { get; set; }

    [Required]
    public string Name { get; set; }

    [Required]
    [EmailAddress]
    public string Email { get; set; }

    public virtual Location Location { get; set; }
  }
}