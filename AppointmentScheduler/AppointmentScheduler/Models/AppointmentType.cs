﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Models
{
  public class AppointmentType
  {
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int? AppointmentTypeId { get; set; }

    [Required]
    public string Name { get; set; }
  }
}