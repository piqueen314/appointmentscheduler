﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Models
{
  public class Location
  {
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int? LocationId { get; set; }

    public int AddressId { get; set; }

    [Required]
    public string Name { get; set; }

    public virtual Address Address { get; set; }
  }
}