﻿using AppointmentScheduler.Models;
using AppointmentScheduler.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.App_Start
{
  public class MapperConfig
  {
    public static void RegisterMaps()
    {
      Mapper.CreateMap<Appointment, AppointmentView>();
      Mapper.CreateMap<AppointmentView, Appointment>();
      Mapper.CreateMap<Customer, CustomerView>();
      Mapper.CreateMap<CustomerView, Customer>();
      Mapper.CreateMap<Location, LocationView>();
      Mapper.CreateMap<LocationView, Location>();
      Mapper.CreateMap<Address, AddressView>();
      Mapper.CreateMap<AddressView, Address>();
    }
  }
}
