﻿using System.Web;
using System.Web.Optimization;

namespace AppointmentScheduler
{
  public class BundleConfig
  {
    private static readonly string[] Jquery = 
    {
      "~/Scripts/jquery-{version}.js"
    };

    private static readonly string[] JqueryVal = 
    {
      "~/Scripts/jquery.validate*"
    };

    ///// <summary>
    ///// Use the development version of Modernizr to develop with and learn from. Then, when you're
    ///// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
    ///// </summary>
    //private static readonly string[] Modernizr = 
    //{
    //  "~/Scripts/modernizr-*"
    //};

    //private static readonly string[] Bootstrap = 
    //{
    //  "~/Scripts/bootstrap*",
    //  "~/Scripts/respond*"
    //};

    private static readonly string[] BootstrapCss = 
    {
      "~/Content/bootstrap.css"
      //"~/Content/site.css"
    };

    private static readonly string[] Utility = 
    {
      "~/Scripts/Utility/DropDownList*",
      "~/Scripts/jquery.maskedinput*"
    };

    private static readonly string[] ApptCreate = 
    {
      "~/Scripts/Views/Appointment/Create*"
    };

    private static readonly string[] ApptTimeDate = 
    {
      "~/Scripts/Views/Appointment/TimeDate*",
      "~/Scripts/bootstrap-datetimepicker*"
    };

    private static readonly string[] DateTimeCss = 
    {
      "~/Content/bootstrap-datetimepicker*",
    };

    private static readonly string[] ApptLocation = 
    {
      "~/Scripts/Views/Appointment/Location*"
    };

    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      // Set EnableOptimizations to false for debugging. For more information,
      // visit http://go.microsoft.com/fwlink/?LinkId=301862
#if DEBUG
      BundleTable.EnableOptimizations = false;
#else
      BundleTable.EnableOptimizations = true;
#endif

      bundles.Add(new StyleBundle("~/Content/DateTime").Include(BootstrapCss).Include(DateTimeCss));

      bundles.Add(new ScriptBundle("~/bundles/Appointment/Create").Include(Utility).Include(ApptCreate));

      bundles.Add(new ScriptBundle("~/bundles/Appointment/Location").Include(Utility).Include(ApptLocation));

      bundles.Add(new ScriptBundle("~/bundles/Appointment/TimeDate").Include(Utility).Include(ApptTimeDate));

      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(Jquery));

      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(JqueryVal));

      //// Use the development version of Modernizr to develop with and learn from. Then, when you're
      //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
      //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(Modernizr));

      //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(Bootstrap));

      //bundles.Add(new StyleBundle("~/Content/css").Include(BootstrapCss));
    }
  }
}
