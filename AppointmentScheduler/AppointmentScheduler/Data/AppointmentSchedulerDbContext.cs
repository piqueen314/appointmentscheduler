﻿using AppointmentScheduler.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.Data
{
  public class AppointmentSchedulerDbContext : DbContext
  {
    public AppointmentSchedulerDbContext()
      : base("AppointmentSchedulerContext")
    {
    }

    public DbSet<Address> Addresses { get; set; }
    public DbSet<Appointment> Appointments { get; set; }
    public DbSet<AppointmentDocument> AppointmentDocuments { get; set; }
    public DbSet<AppointmentType> AppointmentTypes { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<EmployeeContact> EmployeeContacts { get; set; }
    public DbSet<Location> Locations { get; set; }

    //protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //{
    //  modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
    //}
  }
}