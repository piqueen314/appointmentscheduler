﻿using AppointmentScheduler.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System;

namespace AppointmentScheduler.Data
{
  public class AppointmentRepository
  {
    /// <summary>
    /// add a new appointment to the database
    /// </summary>
    /// <param name="appointment"></param>
    /// <returns></returns>
    public Appointment CreateAppointment(Appointment appointment)
    {
      using(var db = new AppointmentSchedulerDbContext())
      {
        db.Appointments.Add(appointment);
        db.SaveChanges();
      }

      return appointment;
    }

    public Appointment GetAppointment(int appointmentId)
    {
      var appt = new Appointment();

      using (var db = new AppointmentSchedulerDbContext())
      {
        appt = GetAppointmentQuery(db).Single(x => x.AppointmentId == appointmentId);
      }

      return appt;
    }

    public List<Location> GetLocations()
    {
      var locations = new List<Location>();

      using (var db = new AppointmentSchedulerDbContext())
      {
        locations = db.Locations.Include(x => x.Address).ToList();
      }

      return locations;
    }

    public List<AppointmentType> GetAppointmentTypes()
    {
      var appointmentTypes = new List<AppointmentType>();

      using (var db = new AppointmentSchedulerDbContext())
      {
        appointmentTypes = db.AppointmentTypes.ToList();
      }

      return appointmentTypes;
    }

    public Location AddLocation(int appointmentId, int locationId)
    {
      var location = new Location();

      using (var db = new AppointmentSchedulerDbContext())
      {
        var appt = GetAppointmentQuery(db).Single(x => x.AppointmentId == appointmentId);

        appt.LocationId = locationId;

        db.SaveChanges();

        location = db.Locations.Include(x => x.Address).Single(x => x.LocationId == locationId);
      }

      return location;
    }

    public Customer AddCustomer(int appointmentId, Customer customer)
    {
      using (var db = new AppointmentSchedulerDbContext())
      {
        var appt = GetAppointmentQuery(db).Single(x => x.AppointmentId == appointmentId);

        appt.Customer = customer;

        db.SaveChanges();
      }

      return customer;
    }

    public Appointment AddStartTime(int appointmentId, DateTime startTime)
    {
      var appointment = new Appointment();

      using (var db = new AppointmentSchedulerDbContext())
      {
        appointment = GetAppointmentQuery(db).Single(x => x.AppointmentId == appointmentId);

        appointment.StartTime = startTime;

        db.SaveChanges();
      }

      return appointment;
    }

    private IQueryable<Appointment> GetAppointmentQuery(AppointmentSchedulerDbContext db)
    {
      return db.Appointments
        .Include(x => x.Customer)
        .Include(x => x.Location)
        .Include(x => x.Location.Address);
    }
  }
}
