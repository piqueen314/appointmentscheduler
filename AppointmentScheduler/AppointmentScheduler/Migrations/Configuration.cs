using AppointmentScheduler.Models;
using AppointmentScheduler.Utility;

namespace AppointmentScheduler.Migrations
{
  using System;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

  internal sealed class Configuration : DbMigrationsConfiguration<AppointmentScheduler.Data.AppointmentSchedulerDbContext>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(AppointmentScheduler.Data.AppointmentSchedulerDbContext context)
    {
      SeedAppointmentTypes(context);
    }

    private void SeedAppointmentTypes(AppointmentScheduler.Data.AppointmentSchedulerDbContext db)
    {
      try
      {
        foreach (Lookups.AppointmentTypes atType in (Lookups.AppointmentTypes[])Enum.GetValues(typeof(Lookups.AppointmentTypes)))
        {
          SeedAppointmentType(db, (int)atType, Lookups.GetDescription(atType));
        }
      }
      catch (Exception)
      {
        throw;
      }
    }

    private void SeedAppointmentType(AppointmentScheduler.Data.AppointmentSchedulerDbContext db, int appointmentTypeId,
      string appointmentTypeName)
    {
      var aType = db.AppointmentTypes.SingleOrDefault(x => x.AppointmentTypeId == appointmentTypeId);

      if (aType == null)
      {
        db.AppointmentTypes.Add(new AppointmentType
        {
          AppointmentTypeId = appointmentTypeId,
          Name = appointmentTypeName
        });
      }
      else
      {
        aType.Name = appointmentTypeName;
      }
    }

  }
}

