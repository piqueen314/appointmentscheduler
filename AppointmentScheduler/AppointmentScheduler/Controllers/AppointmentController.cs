﻿using AppointmentScheduler.Data;
using AppointmentScheduler.Models;
using AppointmentScheduler.Utility;
using AppointmentScheduler.ViewModels;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AppointmentScheduler.Controllers
{
  public class AppointmentController : Controller
  {
    public void TestEmail()
    {
      Mailer.SendEmail("ledawna@gmail.com", "awhite4@gmail.com", "Boo Yah", "Email in your face!");
    }

    public ActionResult Create(int? appointmentId = null)
    {
      var appointmentView = new AppointmentView();

      var appointmentRepo = new AppointmentRepository();

      if (appointmentId.HasValue)
      {
        //load the appointment for editingb
        var appointment = appointmentRepo.GetAppointment(appointmentId.Value);

        appointmentView = Mapper.Map<Appointment, AppointmentView>(appointment);
      }
     
      var appointmentTypes = appointmentRepo.GetAppointmentTypes();

      var appointmentTypeSelectList = appointmentTypes
        .OrderBy(x => x.AppointmentTypeId)
        .Select(x => new SelectListItem
        {
          Text = x.Name,
          Value = x.AppointmentTypeId.ToString()
        })
        .ToList();

      appointmentView.AppointmentTypes = appointmentTypeSelectList;

      return View("Create", appointmentView);
    }

    [HttpPost]
    public ActionResult Create(AppointmentView appointmentView)
    {
      //here's where you save the initial appointment
      var appointment = Mapper.Map<AppointmentView, Appointment>(appointmentView);

      var appointmentRepo = new AppointmentRepository();

      appointment = appointmentRepo.CreateAppointment(appointment);

      appointmentView = Mapper.Map<Appointment, AppointmentView>(appointment);

      return RedirectToAction("Location", "Appointment", new { appointmentView.AppointmentId });
    }

    public ActionResult Location(int appointmentId)
    {
      var appointmentRepo = new AppointmentRepository();
      var appointment = appointmentRepo.GetAppointment(appointmentId);
      var appointmentView = Mapper.Map<Appointment, AppointmentView>(appointment);
      var locations = appointmentRepo.GetLocations();
      
      appointmentView.Locations = Mapper.Map<List<Location>, List<LocationView>>(locations);

      var locationSelectList = locations
        .OrderBy(x => x.Name)
        .Select(x => new SelectListItem
        {
          Text = string.Format("{0}", x.Name),
          Value = x.LocationId.ToString()
        })
        //{
        //  Text = string.Format("{0}, {1}, {2}", x.Name, x.Address.Address1, x.Address.City),
        //  Value = x.LocationId.ToString()
        //})
        .ToList();

      appointmentView.LocationSelectList = locationSelectList;

      return View("Location", appointmentView);
    }

    [HttpPost]
    public ActionResult Location(AppointmentView appointmentView)
    {
      //convert AppointmentView into Appointment
      var appointmentData = Mapper.Map<AppointmentView, Appointment>(appointmentView);

      //new AppointmentRepository
      var appointmentRepo = new AppointmentRepository();

      //add a customer to the appointment
      var locationData = appointmentRepo.AddLocation(appointmentData.AppointmentId, appointmentData.LocationId.Value);

      //update ApointmentView to contain the customer data
      appointmentView.LocationId = locationData.LocationId;
      appointmentView.Location = Mapper.Map<Location, LocationView>(locationData);

      //send the ApointmentView to the verify page
      return RedirectToAction("TimeDate", "Appointment", appointmentView);
    }

    public ActionResult TimeDate(int appointmentId)
    {
      var appointmentRepo = new AppointmentRepository();
      var appointment = appointmentRepo.GetAppointment(appointmentId);
      var appointmentView = Mapper.Map<Appointment, AppointmentView>(appointment);

      return View("TimeDate", appointmentView);
    }

    [HttpPost]
    public ActionResult TimeDate(AppointmentView appointmentView)
    {
      var appointmentRepo = new AppointmentRepository();
      var appointment = appointmentRepo.AddStartTime(appointmentView.AppointmentId, appointmentView.StartTime.Value);

      appointmentView = Mapper.Map<Appointment, AppointmentView>(appointment);

      return RedirectToAction("CustomerInfo", "Appointment", new { appointmentView.AppointmentId });
    }

    public ActionResult CustomerInfo(int appointmentId)
    {
      //var appointmentView = new AppointmentView();

      //return View("CustomerInfo", appointmentView);

      var apptRepo = new AppointmentRepository();
      var appt = apptRepo.GetAppointment(appointmentId);
      var apptView = Mapper.Map<Appointment, AppointmentView>(appt);

      return View("CustomerInfo", apptView);
    }

    [HttpPost]
    public ActionResult CustomerInfo(AppointmentView appointmentView)
    {
      //convert AppointmentView into Appointment
      var appointmentData = Mapper.Map<AppointmentView, Appointment>(appointmentView);
      
      //new AppointmentRepository
      var appointmentRepo = new AppointmentRepository();

      //add a customer to the appointment
      var customerData = appointmentRepo.AddCustomer(appointmentData.AppointmentId, appointmentData.Customer);

      //update ApointmentView to contain the customer data
      appointmentView.CustomerId = customerData.CustomerId;
      appointmentView.Customer = Mapper.Map<Customer, CustomerView>(customerData);

      //send the ApointmentView to the verify page
      return RedirectToAction("Verify", "Appointment", new { appointmentView.AppointmentId });

      //this goes to the Verify action and re-loads the customer from the db
      //return RedirectToAction("Verify", "Appointment");

      //i've had bad luck with this in the past, but it might work
      //return View("Verify", customer);
    }

    public ActionResult Verify(int appointmentId)
    {
      var appointmentRepo = new AppointmentRepository();
      var appointment = appointmentRepo.GetAppointment(appointmentId);
      var appointmentView = Mapper.Map<Appointment, AppointmentView>(appointment);

      return View("Verify", appointmentView);
    }

    [HttpPost]
    public ActionResult Verify(AppointmentView appointmentView)
    {
      //mark the appointment as verified
      //then go somewhere else or whatever

      return null;
    }

    public ActionResult Confirmation()
    {
      return View();
    }
  }
}
