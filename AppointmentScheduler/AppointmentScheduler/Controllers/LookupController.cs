﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppointmentScheduler.Controllers
{
  public class LookupController : Controller
  {
    // GET: Lookup
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Lookup()
    {
      return View();
    }

    public ActionResult Change()
    {
      return View();
    }

    public ActionResult Cancel()
    {
      return View();
    }

    public ActionResult Details()
    {
      return View();
    }

    public ActionResult Reschedule()
    {
      return View();
    }

    public ActionResult Confirmation()
    {
      return View();
    }

    public ActionResult CancelConfirmation()
    {
      return View();
    }
  }
}