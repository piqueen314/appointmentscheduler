﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppointmentScheduler.Controllers
{
  public class AdminController : Controller
  {
    public ActionResult AppointmentInfo()
    {
      return View();
    }

    public ActionResult Cancel()
    {
      return View();
    }

    public ActionResult EditAppointment()
    {
      return View();
    }

    public ActionResult EditBranch()
    {
      return View();
    }

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Reports()
    {
      return View();
    }

    public ActionResult Search()
    {
      return View();
    }
  }
}