﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppointmentScheduler.ViewModels
{
  public class CustomerView
  {
    public int CustomerId { get; set; }

    public bool IsCurrentCustomer { get; set; }

    [DisplayName("First Name:")]
    [Required]
    public string FirstName { get; set; }

    [DisplayName("Last Name:")]
    [Required]
    public string LastName { get; set; }

    [DisplayName("Email:")]
    [Required]
    [EmailAddress]
    public string Email { get; set; }

    [DisplayName("Phone #:")]
    [Required]
    public string Phone { get; set; }
  }
}
