﻿using System.Web.Mvc;
using AppointmentScheduler.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace AppointmentScheduler.ViewModels
{
  public class LocationView
  {
    public int LocationId { get; set; }

    public int AddressId { get; set; }

    [DisplayName("Branch:")]
    [Required]
    public string Name { get; set; }

    [DisplayName("Address:")]
    public AddressView Address { get; set; }
  }
}