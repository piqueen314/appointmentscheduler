﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace AppointmentScheduler.ViewModels
{
  public class AppointmentView
  {
    public int AppointmentId { get; set; }

    public int? AppointmentTypeId { get; set; }

    public int? CustomerId { get; set; }

    public int? LocationId { get; set; }

    [DisplayName("Date & Time:")]
    public DateTime? StartTime { get; set; }

    [DisplayName("Notes:")]
    public string Notes { get; set; }

    [DisplayName("Reason for Appointment:")]
    public IEnumerable<SelectListItem> AppointmentTypes { get; set; }

    public CustomerView Customer { get; set; }

    public LocationView Location { get; set; }

    public List<LocationView> Locations { get; set; }

    public IEnumerable<SelectListItem> LocationSelectList { get; set; }
  }
}
